/**
 * Created with JetBrains WebStorm.
 * User: Ricky
 * Date: 7/31/13
 * Time: 10:17 PM
 * To change this template use File | Settings | File Templates.
 */
var http = require("http");
var static = require("node-static");
fs = new static.Server();  //Chooses the directory to serve when node is running
var server = http.createServer(function(request, response){
     fs.serve(request, response);
}).listen(1337);

var io = require("socket.io").listen(server);
io.sockets.on("connection", function(socket){
     socket.on("click", function(data){
         io.sockets.emit("click", data);
     })

    socket.on("clearscreen", function(data){
        io.sockets.emit("clearscreen", data);
    })

    socket.on("move", function(data){
        io.sockets.emit("move", data);
    })
})
