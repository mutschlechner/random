//Returns a pseudorandom number based on a user-defined seed.
Math.seededRandom = function(min, max, seed, quantity) {
    Math.seed = seed;
    max = max || 1;
    min = min || 0;

    var temp = [quantity];

    for(var i = 0; i < quantity; i++){
        Math.seed = (Math.seed * 9301 + 49297) % 233280;
        var rnd = Math.seed / 233280;
        temp[i] = (min + rnd * (max - min));
    }


    return temp;
    //return min + rnd * (max - min);
}