
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    ctx.fillStyle = "rgb(0, 0, 0)";
    ctx.fillRect(0, 0, c.width, c.height);
    var tempSeed = 5;
    var tempSquare = {x: 100, y: 100};

    //Begin KeyStates:
    var keyStates = {
        87: "keyup", //up
        83: "keyup", //down
        65: "keyup", //left
        68: "keyup", //right
        32: "keyup" //space
    };
    //End KeyStates

    //setting up our socket
    var socket = io.connect(window.location.href);
    socket.on("click", function(data){
        generateParticleExplosion(data);
        console.log(data);
    })

    socket.on("move", function(data){
        tempSquare = data;
        move(tempSquare);
        console.log("Moving Square!");
    })

    socket.on("clearscreen", function(data){
        reset();
        //duoieie
    })

    var particles = []
    var mouse = {
        x: c.width/2,
        y: c.height/2,
        size: 50
    }


    function Particle(position, velocity, color, size, sizeVelocity){
        this.position = position;
        this.velocity = velocity;
        this.color = color;
        this.size = size;
        this.sizeVelocity = sizeVelocity;
    }



    function loop(){
        update();
        render();
        window.requestAnimationFrame(loop);
    }



    function reset(){
       particles = [];
    }

    function update(){
      for(var i = 0; i < particles.length; i++){
          if(particles[i].position.x < 0 || particles[i].position.x > c.width){
              particles[i].velocity.x *= -1;
          }

          if(particles[i].position.y < 0 || particles[i].position.y > c.height){
              particles[i].velocity.y *= -1;
          }
            particles[i].position.x += particles[i].velocity.x;
            particles[i].position.y += particles[i].velocity.y;

      }

        if(particles.length > 1500){
            particles.splice(0, 100);
        }
    }




    function render(){
        ctx.fillStyle = "rgba(0, 0, 0, 0.2)";
        ctx.fillRect(0, 0, c.width, c.height);

        drawRect(tempSquare);

        for(var i = 0; i < particles.length; i++){
            ctx.fillStyle = particles[i].color;
            ctx.beginPath();
            ctx.arc(particles[i].position.x, particles[i].position.y, particles[i].size, 0, Math.PI*2);
            ctx.closePath();
            ctx.fill();
        }
    }

    function randomColor(tempSeed){
        var temp = Math.seededRandom(0, 255, tempSeed, 3);
        var r = temp[0];
        var g = temp[1];
        var b = temp[2];
        var a = Math.seededRandom(0, 1, tempSeed, 1);

        return ("rgba(" + Math.floor(r) + "," + Math.floor(g) + "," + Math.floor(b) + "," + a + ")");
    }

    function mouseMove(e){
        mouse.x = e.pageX;
        mouse.y = e.pageY;
    }

    function mouseDown(e){
        socket.emit("click", {pos:mouse, seed:Math.random()});
    }

    function keyPressed(e){
        console.log(e.type);
        keyStates[e.keyCode] = e.type;

        socket.emit("move", tempSquare);

        if(keyStates[32] == "keydown"){
            socket.emit("clearscreen", {});
        }
    }


    window.addEventListener("keyup", keyPressed, true);
    window.addEventListener("keydown", keyPressed, true);

    window.addEventListener("mousemove", mouseMove, true);
    window.addEventListener("mousedown", mouseDown, true);


    function generateParticleExplosion(data){
        var tempSeed = data.seed;
        var pos = data.pos;
        var randVelocity = {
                        x: Math.seededRandom(-10, 10, tempSeed, 50),
                        y: Math.seededRandom(-10, 10, tempSeed+29, 50)
                       };


    var randSize = Math.seededRandom(0, 15, tempSeed+5, 50);

    var randColors = Math.seededRandom(0, 4000, tempSeed+72, 50);

    for(var i = 0; i < 50; i++){
        //console.log(Math.seededRandom(-10, 10, tempSeed));
        //particles.push(new Particle({x: Math.random() * c.width, y:Math.random() * c.height}, {x: (Math.random()-Math.random()) * 10, y:(Math.random()-Math.random()) * 10}, randomColor(), Math.random() * 10, Math.random() * 5));
        particles.push(new Particle(JSON.parse(JSON.stringify(pos)), {x: randVelocity.x[i], y: randVelocity.y[i]}, randomColor(randColors[i]),
        randSize[i], 0));
        //console.log(particles);
    }

    }

    function drawRect(square){
        ctx.fillStyle="rgb(0,255,0)"
        ctx.fillRect(square.x,square.y,50,50)
    }

    function move(obj){
        if(keyStates[68] == "keydown"){
            //console.log("Moving right");
            obj.x += 3;
        }

        if(keyStates[65] == "keydown"){
            //console.log("Moving left");
            obj.x -= 3;
        }

        if(keyStates[83] == "keydown"){
            //console.log("Moving down");
            obj.y += 3;
        }

        if(keyStates[87] == "keydown"){
            //console.log("Moving up");
            obj.y -= 3;
        }
    }


    window.requestAnimationFrame(loop);